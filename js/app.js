let userNumberFirst = prompt("Введіть перше число:");
while (isNaN(userNumberFirst)) {
    userNumberFirst = prompt("Це не число. Введіть перше число: ");
}

let userNumberSecond = prompt("Введіть друге число:");
while (isNaN(userNumberSecond)) {
    userNumberSecond = prompt("Це не число. Введіть друге число:");
}

let numberSmaller; 
let numberBigger;

if (userNumberFirst < userNumberSecond) {
    numberSmaller = userNumberFirst;
    numberBigger = userNumberSecond;
} else {
    numberSmaller = userNumberSecond;
    numberBigger = userNumberFirst;
}

let numberString = "";

for (let number = numberSmaller; number <= numberBigger; number++) {
    numberString += number + " ";
}
alert(numberString);





let evenNumber = prompt("Введіть число:");

while (evenNumber % 2 !== 0) {
    evenNumber = prompt("Число не парне. Введіть число:");
}

alert(evenNumber + " парне число");